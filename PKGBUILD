# Maintainer: Michal Sotolar <michal@sotolar.com>
# Credit goes also to the maintainers and contributors of other PHP versions in
# AUR or official Arch Linux repositories. Specific patches might include code
# from other open source projects. In that case, credit is attributed in the
# specific commit.

pkgbase=php56
_pkgbase=${pkgbase%56}

pkgname=("${pkgbase}"
         "${pkgbase}-cgi"
         "${pkgbase}-apache"
         "${pkgbase}-fpm"
         "${pkgbase}-embed"
         "${pkgbase}-phpdbg"
         "${pkgbase}-dblib"
         "${pkgbase}-pear"
         "${pkgbase}-enchant"
         "${pkgbase}-gd"
         "${pkgbase}-imap"
         "${pkgbase}-intl"
         "${pkgbase}-ldap"
         "${pkgbase}-mcrypt"
         "${pkgbase}-mssql"
         "${pkgbase}-odbc"
         "${pkgbase}-pgsql"
         "${pkgbase}-pspell"
         "${pkgbase}-snmp"
         "${pkgbase}-sqlite"
         "${pkgbase}-tidy"
         "${pkgbase}-xsl")

pkgver=5.6.40
pkgrel=6
pkgdesc="A general-purpose scripting language that is especially suited to web development"
arch=('x86_64')
url="http://php.net"
license=('PHP')

makedepends=('apache' 'c-client' 'postgresql-libs' 'libldap' 'smtp-forwarder'
             'sqlite' 'unixodbc' 'net-snmp' 'libzip' 'enchant' 'file' 'freetds'
             'libmcrypt' 'tidyhtml' 'aspell' 'libltdl' 'gd' 'icu' 'curl'
             'libxslt' 'openssl-1.0' 'db' 'gmp' 'systemd' 'libnsl')

source=("https://secure.php.net/distributions/${_pkgbase}-${pkgver}.tar.xz"
        "https://secure.php.net/distributions/${_pkgbase}-${pkgver}.tar.xz.asc"
        'php.ini.patch' 'apache.conf' 'icu-70.patch' 'mysqlnd-fix.patch' 
        'php-fpm.conf.in.patch' 'logrotate.d.php-fpm' 'php-fpm.service' 
        'php-fpm.tmpfiles' 'use-enchant2.patch' 'php-freetype-2.9.1.patch')

sha256sums=('1369a51eee3995d7fbd1c5342e5cc917760e276d561595b6052b21ace2656d1c'
            'SKIP'
            'f70833674fec2575bb404a907fb01c616537629d6a4bfa7c319df04fb90e21e1'
            '8b5b15f1c348d8897d837ea9894157d9630dc542bbb0dbc7ad93c5dc0235d1d5'
            '5dbc2d5f3bcfe52d9717d0c9ad42f13f6a56917628afc932c6715e2767b7033f'
            '4599f34603b8af7c33b123bc14d2cea5832fe2275c901cbd46ff76182b1dbf13'
            '71996465bf1ccd0f335cd023f74dc362c9ac8eae03ce400dc36a729d42431403'
            'e1eee1e035a3ae973c08989564227577d996fddace2bb25d0c62ef8763d109a8'
            'c65c3c140aa06868ecef672c17b467589dc03d21caf06a75511dbc19c0bae1c3'
            '2fb61bcef63765d3491bb78cab835f1ab6819c127bef4d30945916193608eb4e'
            '471eadcbe1d28102774260fa7fcb47fb65b104c4e3fc7267c3ae59e075c8cceb'
            'f9fe57f809ac13e0043d18b795ef777af3e8c710a83745b37b09db536f683d2a')

validpgpkeys=('6E4F6AB321FDC07F2C332E3AC2BF0BC433CFC8B3'
              '0BD78B5F97500D450838F95DFE857D9A90D90EC1')

prepare() {
        cd ${srcdir}/${_pkgbase}-${pkgver}

        patch -p0 -i "${srcdir}/php.ini.patch"
        patch -p0 -i "${srcdir}/php-fpm.conf.in.patch"

        sed '/APACHE_THREADED_MPM=/d' -i sapi/apache2handler/config.m4 -i configure
        sed 's/buffio\.h/tidybuffio\.h/' -i ext/tidy/tidy.c

        patch -p0 -N -l -i "${srcdir}/use-enchant2.patch"
        patch -p1 -N -l -i "${srcdir}/php-freetype-2.9.1.patch"
        patch -p1 -N -l -i "${srcdir}/icu-70.patch"
        patch -p1 -N -l -u -i "${srcdir}/mysqlnd-fix.patch"
}

build() {
        CPPFLAGS+=' -DU_USING_ICU_NAMESPACE=1'
        CPPFLAGS+=' -DU_DEFINE_FALSE_AND_TRUE=1'

        local _phpconfig=" \
                --srcdir=../${_pkgbase}-${pkgver} \
                --config-cache \
                --prefix=/usr \
                --sysconfdir=/etc/${pkgbase} \
                --localstatedir=/var \
                --libdir=/usr/lib/${pkgbase} \
                --datarootdir=/usr/share/${pkgbase} \
                --datadir=/usr/share/${pkgbase} \
                --program-suffix=${pkgbase#php} \
                --with-layout=GNU \
                --with-config-file-path=/etc/${pkgbase} \
                --with-config-file-scan-dir=/etc/${pkgbase}/conf.d \
                --disable-rpath \
                --without-pear \
                "

        local _phpextensions=" \
                --enable-bcmath=shared \
                --enable-calendar=shared \
                --enable-dba=shared \
                --enable-exif=shared \
                --enable-ftp=shared \
                --enable-gd-native-ttf \
                --enable-intl=shared \
                --enable-mbstring \
                --enable-opcache \
                --enable-phar=shared \
                --enable-posix=shared \
                --enable-shmop=shared \
                --enable-soap=shared \
                --enable-sockets=shared \
                --enable-sysvmsg=shared \
                --enable-sysvsem=shared \
                --enable-sysvshm=shared \
                --enable-zip=shared \
                --with-bz2=shared \
                --with-curl=shared \
                --with-db4=/usr \
                --with-enchant=shared,/usr \
                --with-fpm-systemd \
                --with-freetype-dir=/usr \
                --with-xpm-dir=/usr \
                --with-gd=shared,/usr \
                --with-gdbm \
                --with-gettext=shared \
                --with-gmp=shared \
                --with-iconv=shared \
                --with-icu-dir=/usr \
                --with-imap-ssl \
                --with-imap=shared \
                --with-kerberos=/usr \
                --with-jpeg-dir=/usr \
                --with-vpx-dir=no \
                --with-ldap=shared \
                --with-ldap-sasl \
                --with-libzip \
                --with-mcrypt=shared \
                --with-mhash \
                --with-mssql=shared \
                --with-mysql-sock=/run/mysqld/mysqld.sock \
                --with-mysql=shared,mysqlnd \
                --with-mysqli=shared,mysqlnd \
                --with-openssl=shared \
                --with-pcre-regex=/usr \
                --with-pdo-dblib=shared,/usr \
                --with-pdo-mysql=shared,mysqlnd \
                --with-pdo-odbc=shared,unixODBC,/usr \
                --with-pdo-pgsql=shared \
                --with-pdo-sqlite=shared,/usr \
                --with-pgsql=shared \
                --with-png-dir=/usr \
                --with-pspell=shared \
                --with-snmp=shared \
                --with-sqlite3=shared,/usr \
                --with-tidy=shared \
                --with-unixODBC=shared,/usr \
                --with-xmlrpc=shared \
                --with-xsl=shared \
                --with-zlib \
                "

        export EXTENSION_DIR="/usr/lib/${pkgbase}/modules"
        export PEAR_INSTALLDIR="/usr/share/${pkgbase}/pear"
        export PKG_CONFIG_PATH=/usr/lib/openssl-1.0/pkgconfig

        cd "${srcdir}/${_pkgbase}-${pkgver}"

        # php
        mkdir -p "${srcdir}/build-php"
        cd "${srcdir}/build-php"
        ln -sf ../${_pkgbase}-${pkgver}/configure
        ./configure ${_phpconfig} \
                --disable-cgi \
                --with-readline \
                --enable-pcntl \
                ${_phpextensions}
        sed -i '/^IMAP_SHARED_LIBADD =/ s#-lssl -lcrypto#-Wl,/usr/lib/libssl.so -Wl,/usr/lib/libcrypto.so#' Makefile
        make -s

        # cgi and fcgi
        cp -Ta ${srcdir}/build-php ${srcdir}/build-cgi
        cd ${srcdir}/build-cgi
        ./configure ${_phpconfig} \
                --disable-cli \
                --enable-cgi \
                ${_phpextensions}
        make -s

        # apache
        cp -Ta ${srcdir}/build-php ${srcdir}/build-apache
        cd ${srcdir}/build-apache
        ./configure ${_phpconfig} \
                --disable-cli \
                --with-apxs2 \
                ${_phpextensions}
        make -s

        # fpm
        cp -Ta ${srcdir}/build-php ${srcdir}/build-fpm
        cd ${srcdir}/build-fpm
        ./configure ${_phpconfig} \
                --disable-cli \
                --enable-fpm \
                --with-fpm-user=http \
                --with-fpm-group=http \
                ${_phpextensions}
        make -s

        # embed
        cp -Ta ${srcdir}/build-php ${srcdir}/build-embed
        cd ${srcdir}/build-embed
        ./configure ${_phpconfig} \
                --disable-cli \
                --enable-embed=shared \
                ${_phpextensions}
        make -s

        # phpdbg
        cp -Ta ${srcdir}/build-php ${srcdir}/build-phpdbg
        cd ${srcdir}/build-phpdbg
        ./configure ${_phpconfig} \
                --disable-cli \
                --disable-cgi \
                --with-readline \
                --enable-phpdbg \
                ${_phpextensions}
        make -s

        # pear
        sed -i 's#@$(top_builddir)/sapi/cli/php $(PEAR_INSTALL_FLAGS) pear/install-pear-nozlib.phar -d#@$(top_builddir)/sapi/cli/php $(PEAR_INSTALL_FLAGS) pear/install-pear-nozlib.phar -p $(bindir)/php$(program_suffix) -d#' ${srcdir}/php-${pkgver}/pear/Makefile.frag
        cp -Ta ${srcdir}/build-php ${srcdir}/build-pear
        cd ${srcdir}/build-pear
                ./configure ${_phpconfig} \
                --disable-cgi \
                --with-readline \
                --enable-pcntl \
                --with-pear \
                ${_phpextensions}
        make -s
}

check() {
        cd "${srcdir}/${_pkgbase}-${pkgver}"

        # Check if sendmail was configured correctly (FS#47600)
        "${srcdir}"/build-php/sapi/cli/php -n -r 'echo ini_get("sendmail_path");' | grep -q 'sendmail'

        export REPORT_EXIT_STATUS=1
        export NO_INTERACTION=1
        export SKIP_ONLINE_TESTS=1
        export SKIP_SLOW_TESTS=1

        "${srcdir}"/build-php/sapi/cli/php -n run-tests.php -n -P tests
}

package_php56() {
        pkgdesc='A general-purpose scripting language that is especially suited to web development'
        depends=('pcre' 'libxml2' 'curl' 'libzip' 'openssl-1.0')
        provides=("${_pkgbase}=$pkgver")
        backup=("etc/${pkgbase}/php.ini")

        cd ${srcdir}/build-php
        make -j1 INSTALL_ROOT=${pkgdir} install

        install -D -m644 ${srcdir}/${_pkgbase}-${pkgver}/php.ini-production ${pkgdir}/etc/${pkgbase}/php.ini
        install -d -m755 ${pkgdir}/etc/${pkgbase}/conf.d/

        rm -f ${pkgdir}/usr/lib/${pkgbase}/modules/*.a
        rm -f ${pkgdir}/usr/lib/${pkgbase}/modules/{enchant,gd,imap,intl,ldap,mcrypt,mssql,odbc,pdo_odbc,pgsql,pdo_pgsql,pspell,snmp,sqlite3,pdo_sqlite,tidy,xsl,pdo_dblib}.so

        rmdir ${pkgdir}/usr/include/php/include
        mv ${pkgdir}/usr/include/php ${pkgdir}/usr/include/${pkgbase}

        rm ${pkgdir}/usr/bin/phar
        ln -sf phar.${pkgbase/php/phar} ${pkgdir}/usr/bin/${pkgbase/php/phar}

        mv ${pkgdir}/usr/bin/phar.{phar,${pkgbase/php/phar}}
        mv ${pkgdir}/usr/share/man/man1/{phar,${pkgbase/php/phar}}.1
        mv ${pkgdir}/usr/share/man/man1/phar.{phar,${pkgbase/php/phar}}.1

        sed -i "/^includedir=/c \includedir=/usr/include/${pkgbase}" ${pkgdir}/usr/bin/${pkgbase/php/phpize}
        sed -i "/^include_dir=/c \include_dir=/usr/include/${pkgbase}" ${pkgdir}/usr/bin/${pkgbase/php/php-config}
        sed -i "/^\[  --with-php-config=/c \[  --with-php-config=PATH  Path to php-config [${pkgbase/php/php-config}]], ${pkgbase/php/php-config}, no)" ${pkgdir}/usr/lib/${pkgbase}/build/phpize.m4
}

package_php56-cgi() {
        pkgdesc='CGI and FCGI SAPI for PHP'
        depends=("${pkgbase}")
        provides=("${_pkgbase}-cgi=$pkgver")

        install -D -m755 ${srcdir}/build-cgi/sapi/cgi/php-cgi ${pkgdir}/usr/bin/${pkgbase}-cgi
}

package_php56-apache() {
        pkgdesc='Apache SAPI for PHP'
        depends=("${pkgbase}" 'apache' 'libnsl')
        provides=("${_pkgbase}-apache=$pkgver")
        backup=("etc/httpd/conf/extra/${pkgbase}_module.conf")
        install='php-apache.install'

        install -D -m755 ${srcdir}/build-apache/libs/libphp5.so ${pkgdir}/usr/lib/httpd/modules/lib${pkgbase}.so
        install -D -m644 ${srcdir}/apache.conf ${pkgdir}/etc/httpd/conf/extra/${pkgbase}_module.conf
}

package_php56-fpm() {
        pkgdesc='FastCGI Process Manager for PHP'
        depends=("${pkgbase}" 'systemd')
        provides=("${_pkgbase}-fpm=$pkgver")
        backup=("etc/${pkgbase}/php-fpm.conf")
        install='php-fpm.install'

        install -d -m755 ${pkgdir}/usr/binfpm.d
        install -D -m755 ${srcdir}/build-fpm/sapi/fpm/php-fpm ${pkgdir}/usr/bin/${pkgbase}-fpm

        install -D -m644 ${srcdir}/build-fpm/sapi/fpm/php-fpm.8 ${pkgdir}/usr/share/man/man8/${pkgbase}-fpm.8
        install -D -m644 ${srcdir}/build-fpm/sapi/fpm/php-fpm.conf ${pkgdir}/etc/${pkgbase}/php-fpm.conf

        install -d -m755 ${pkgdir}/etc/${pkgbase}/fpm.d
        install -D -m644 ${srcdir}/php-fpm.tmpfiles ${pkgdir}/usr/lib/tmpfiles.d/${pkgbase}-fpm.conf
        install -D -m644 ${srcdir}/php-fpm.service ${pkgdir}/usr/lib/systemd/system/${pkgbase}-fpm.service

        install -d -m755 ${pkgdir}/etc/logrotate.d
        install -D -m644 ${srcdir}/logrotate.d.php-fpm ${pkgdir}/etc/logrotate.d/${pkgbase}-fpm
}

package_php56-embed() {
        pkgdesc='Embedded PHP SAPI library'
        depends=("${pkgbase}" 'libnsl')
        provides=("${_pkgbase}-embed=$pkgver")

        install -D -m755 ${srcdir}/build-embed/libs/libphp5.so ${pkgdir}/usr/lib/libphp56.so
        install -D -m644 ${srcdir}/${_pkgbase}-${pkgver}/sapi/embed/php_embed.h ${pkgdir}/usr/include/${pkgbase}/sapi/embed/php_embed.h
}

package_php56-phpdbg() {
        pkgdesc='Interactive PHP debugger'
        depends=("${pkgbase}")
        provides=("${_pkgbase}-phpdbg=$pkgver")

        install -d -m755 ${pkgdir}/usr/bin
        install -D -m755 ${srcdir}/build-phpdbg/sapi/phpdbg/phpdbg ${pkgdir}/usr/bin/${pkgbase}dbg
}

package_php56-dblib() {
        pkgdesc='dblib module for PHP'
        depends=("${pkgbase}" 'freetds')
        provides=("${_pkgbase}-dblib=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/pdo_dblib.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_dblib.so
}

package_php56-pear() {
        pkgdesc='PHP Extension and Application Repository'
        depends=("${pkgbase}")
        provides=("${_pkgbase}-pear=$pkgver")
        backup=("etc/${pkgbase}/pear.conf")

        cd ${srcdir}/build-pear
        make -j1 INSTALL_ROOT=${pkgdir} install-pear
        rm -rf ${pkgdir}{/usr/share/${pkgbase}/pear,}/.{channels,depdb,depdblock,filemap,lock,registry}

        mv ${pkgdir}/usr/bin/{pear,${pkgbase/php/pear}}
        mv ${pkgdir}/usr/bin/{peardev,${pkgbase/php/peardev}}
        mv ${pkgdir}/usr/bin/{pecl,${pkgbase/php/pecl}}
}

package_php56-enchant() {
        pkgdesc='enchant module for PHP'
        depends=("${pkgbase}" 'enchant')
        provides=("${_pkgbase}-enchant=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/enchant.so ${pkgdir}/usr/lib/${pkgbase}/modules/enchant.so
}

package_php56-gd() {
        pkgdesc='gd module for PHP'
        depends=("${pkgbase}" 'gd')
        provides=("${_pkgbase}-gd=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/gd.so ${pkgdir}/usr/lib/${pkgbase}/modules/gd.so
}

package_php56-imap() {
        pkgdesc='imap module for PHP'
        depends=("${pkgbase}" 'c-client')
        provides=("${_pkgbase}-imap=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/imap.so ${pkgdir}/usr/lib/${pkgbase}/modules/imap.so
}

package_php56-intl() {
        pkgdesc='intl module for PHP'
        depends=("${pkgbase}" 'icu')
        provides=("${_pkgbase}-intl=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/intl.so ${pkgdir}/usr/lib/${pkgbase}/modules/intl.so
}

package_php56-ldap() {
        pkgdesc='ldap module for PHP'
        depends=("${pkgbase}" 'libldap')
        provides=("${pkgbase}-ldap=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/ldap.so ${pkgdir}/usr/lib/${pkgbase}/modules/ldap.so
}

package_php56-mcrypt() {
        pkgdesc='mcrypt module for PHP'
        depends=("${pkgbase}" 'libmcrypt' 'libltdl')
        provides=("${_pkgbase}-mcrypt=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/mcrypt.so ${pkgdir}/usr/lib/${pkgbase}/modules/mcrypt.so
}

package_php56-mssql() {
        pkgdesc='mssql module for PHP'
        depends=("${pkgbase}" 'freetds')
        provides=("${_pkgbase}-mssql=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/mssql.so ${pkgdir}/usr/lib/${pkgbase}/modules/mssql.so
}

package_php56-odbc() {
        pkgdesc='ODBC modules for PHP'
        depends=("${pkgbase}" 'unixodbc')
        provides=("${_pkgbase}-odbc=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/odbc.so ${pkgdir}/usr/lib/${pkgbase}/modules/odbc.so
        install -D -m755 ${srcdir}/build-php/modules/pdo_odbc.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_odbc.so
}

package_php56-pgsql() {
        pkgdesc='PostgreSQL modules for PHP'
        depends=("${pkgbase}" 'postgresql-libs')
        provides=("${_pkgbase}-pgsql=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/pgsql.so ${pkgdir}/usr/lib/${pkgbase}/modules/pgsql.so
        install -D -m755 ${srcdir}/build-php/modules/pdo_pgsql.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_pgsql.so
}

package_php56-pspell() {
        pkgdesc='pspell module for PHP'
        depends=("${pkgbase}" 'aspell')
        provides=("${_pkgbase}-pspell=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/pspell.so ${pkgdir}/usr/lib/${pkgbase}/modules/pspell.so
}

package_php56-snmp() {
        pkgdesc='snmp module for PHP'
        depends=("${pkgbase}" 'net-snmp')
        provides=("${_pkgbase}-snmp=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/snmp.so ${pkgdir}/usr/lib/${pkgbase}/modules/snmp.so
}

package_php56-sqlite() {
        pkgdesc='sqlite module for PHP'
        depends=("${pkgbase}" 'sqlite')
        provides=("${_pkgbase}-sqlite=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/sqlite3.so ${pkgdir}/usr/lib/${pkgbase}/modules/sqlite3.so
        install -D -m755 ${srcdir}/build-php/modules/pdo_sqlite.so ${pkgdir}/usr/lib/${pkgbase}/modules/pdo_sqlite.so
}

package_php56-tidy() {
        pkgdesc='tidy module for PHP'
        depends=("${pkgbase}" 'tidyhtml')
        provides=("${_pkgbase}-tidy=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/tidy.so ${pkgdir}/usr/lib/${pkgbase}/modules/tidy.so
}

package_php56-xsl() {
        pkgdesc='xsl module for PHP'
        depends=("${pkgbase}" 'libxslt')
        provides=("${_pkgbase}-xsl=$pkgver")

        install -D -m755 ${srcdir}/build-php/modules/xsl.so ${pkgdir}/usr/lib/${pkgbase}/modules/xsl.so
}
